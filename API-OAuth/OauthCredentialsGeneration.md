# Gerando chaves OAuth para Hub2b API externamente

A possibilidade de gerar chaves de acesso à API da Hub2b externamente é direcionada para integrações onde o marketplace ou o ERP integra com a Hub2b para acessar os dados de nossos clientes. 

Para que isso seja possível é necessário entrar em contato com nossa equipe para criarmos uma Aplicação para o sistema. Essa aplicação consiste em gerar duas cradenciais para o sistema: 

 - client_id 
 - client_secret

Essas credenciais servirão para que a Hub2b possa identificar quem está realizando as ações em nossa API. Para solicitar a criação de uma aplicação o sistema deverá nos informar uma rota de redirecionamento para que a Hub2b redirecione as chaves de acesso criadas após o final de processo. Essa rota será explicada posteriomente. 

Quando o sistema deseja gerar as chaves de acesso para integração de um determinado cliente ele deverá acessar a seguinte rota em nossa plataforma: 

```
https://app.hub2b.com.br/site/authorization?client_id=<client_id>&scope=inventory%20orders
```

Onde o client_id identifica o sistema que deseja ter acesso a determinada conta. 

Nesta rota, o usuario deverá digitar seu usuario e senha da plataforma para delegar acesso ao sistema. 

![login_oauth](img/login_oauth.png)

Após realizar o login, o cliente deverá selecionar a conta que deseja dar acesso, ao clicar no idTenant, ele será enviado para a seguinte tela: 

![authorization_login](img/authorization_login.png)

O cliente deverá clicar em autorizar. Neste momento, a página será redirecionada para a rota de redirecionamento citada no inicio deste fluxo com um paramentro "code" na url. 

Exemplo:

```
https://url_do_seu_site/alguma_rota?code=<codigo_de_acesso>
```

Esse parametro deverá ser utilizado para gerar o access_token e refresh_token do cliente. Para isso, deve ser realizada a seguinte requisição: 

```
curl --header "Content-Type:application/json"
    --request POST
    --data '
        {
            "client_id": "<seu_client_id>",
            "grant_type": "authorization_code",
            "client_secret": "<seu_client_secret>"
        }
    '
    https://rest.hub2b.com.br/oauth2/token?code=<codigo_gerado>

```

o retorno desta chama ser á o seguinte: 

```
{
  "refresh_token": "<seu_refresh_toke_atual>",
  "token_type": "bearer",
  "access_token": "<seu_access_token_atual>",
  "expires_in": 7200
}
```

Com essas informações, basta seguir o passo a passo descrito em [Autenticações](../authentications.md).