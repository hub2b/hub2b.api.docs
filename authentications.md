# Métodos de autenticação da APIs Hub2b

<p align="justify">
Por conta da existencia de diferentes modelos de APIs e questões como dívida tecnica, temos dois modelos de autenticação de API e ambos não funcionam para todas as APIs, sendo assim, é necessário utilizar um modelo de autenticação para algumas APIs e outro modelo para outras APIs. 
</p>

A divisão do uso é simples: 

- Recursos da API v1 utilizam autenticação Chave/Valor
- Demais recursos utilizam autenticação oauth2

<p align="justify">
Para que seja possível gerar as chaves para testes ou para a integração, é necessário que o usuário já tenha uma conta criada na Hub2b. O cadastro deve ser realizado em <a href="https://app.hub2b.com.br/signup">https://app.hub2b.com.br/signup</a>. Após o cadastro o cliente tem 7 dias úteis para testar os recursos da plataforma. Podendo ter esse tempo extendido para desenvolver a integração. 


Após criar uma conta, é necessário entrar em contato com nossa equipe para que a gente repasse as chaves de acesso.
</p>

## Autenticação Chave/Valor

Esse método de autenticação é utilizado para acessar os recursos da [API v1](https://hub2bapi.docs.apiary.io/#).

Ele consiste em passar um header nas requisições REST da API no seguinte formato:

```
auth: <seu_token>
```

exemplo de requisição:

```
curl --header "auth:<seu_token>" \
    --request GET \
    https://eb-api.plataformahub.com.br/RestServiceImpl.svc/listskus/<id_conta>?filter=&offset=0&limit=20 
```

Para obter um token chave/valor para integração com a [API v1](https://hub2bapi.docs.apiary.io/#), é necessário entrar em contato com nossa equipe.


## Autenticação OAuth2

Este método é utilizado para todas as demais APIs da Hub2b e consiste em passar na query da requisição um parametro "access_token". Este access token é temporário e deve ser atualizado de 2 em 2 horas utilizando uma chave chamada "refresh_token". 

Para obter seu access_token e refresh_token é necessário ter as seguintes informações: 

 - client_id
 - client_secret
 - user
 - password

Caso você tenha multicontas, ou deseje integrar com mais de um seller, verificar [Gerando chaves externamente](API-OAuth/OauthCredentialsGeneration.md)

Para obter esses dados é necessário entrar em contato com a nossa equipe.

Com esses dados em mãos, para obter o access_token e o refresh_token, deve-se realizar um requisição REST no seguinte formato: 

```
curl --header "Content-Type: application/json" \
    --request POST \
    --data '
        {  \
            "client_id": "<seu_client_id>",  \
            "client_secret": "<seu_client_secret>",  \
            "grant_type": "password",  \
            "scope": "orders",  \
            "username": "<seu_user>",  \
            "password": "<seu_password>"
        }' \
    https://rest.hub2b.com.br/oauth2/login
```

O retorno desta requisição será o seguinte: 

```
{
  "refresh_token": "<seu_refresh_toke_atual>",
  "token_type": "bearer",
  "access_token": "<seu_access_token_atual>",
  "expires_in": 7200
}
```

Você deverá guardar o refresh_token e o access_token, com o access_token você realizará as requisições da API. O access_token expira em 2 horas, após isso deverá ser utilizado o refresh_token na rota POST em https://rest.hub2b.com.br/oauth2/token para renovar seu access_token e voltar a realizar requisições. O refresh do access_token é realizado da seguinte forma: 


```
curl --header "Content-Type: application/json" \
    --request POST \
    --data '
        {  \
            "client_id": "<seu_client_id>",  \
            "client_secret": "<seu_client_secret>",  \
            "grant_type": "refresh_token",  \
            "refresh_token": "<seu_refresh_token_atual>"
        }' \
    https://rest.hub2b.com.br/oauth2/token
```

O retorno desta requisição será o seguinte: 

```
{
  "refresh_token": "<seu_refresh_toke_novo>",
  "token_type": "bearer",
  "access_token": "<seu_access_token_novo>",
  "expires_in": 7200
}
```

A partir de agora, o access_token e o refresh_token mudam. Os dados utilizados anteriormente deixam de ser válidos e você deve utilizar o novo access_token e refresh_token. 

Proximo Passo: [APIv1](APIv1/0.index.md)