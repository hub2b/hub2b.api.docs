# Hub2b APIs
<p align="justify">Em um constante processo de adaptações e melhorias, a Hub2b tem lançado novas APIs com tecnologias de ponta 
para cada um de seus modulos. Hoje, a Hub2b conta com várias APIs onde cada uma delas é especializada para 
um módulo de integração (e.g: Pedidos, Preço/Estoque, Frete, Produtos). A documentação de cada uma dessas APIs 
é disponibilizada em um dominio próprio, contendo as especificações das funcionalidades disponiveis. </p>

<p align="justify">
    Todas as APIs disponibilizadas funcionam no padrão HTTP REST, a documentação de cada uma delas está listada abaixo:
</p>

- [API v1 (deprecated*)](https://hub2bapi.docs.apiary.io/#)
- [API de Pedidos](https://rest.hub2b.com.br/orders-docs/index.html)
- [API de Preço/Estoque](https://rest.hub2b.com.br/inventory-docs/index.html)
- [API de Frete](https://freight.hub2b.com.br/swagger/index.html)

Abaixo é explicado um pouco sobre o uso de cada uma delas:

## API v1
<b>DOCS:</b> <a href="https://hub2bapi.docs.apiary.io/">https://hub2bapi.docs.apiary.io/</a>
<p align="justify">
Essa é a primeira versão de nossa API. É a única API que contém todos os recursos necessários para integração com a 
Hub2b, porém com tecnologia limitada. Essa API hoje é utilizada por sellers apenas para integração de catálogo de 
produtos. Ou seja, os módulos de categorias, sku, status de sku  e remoção de sku. Os demais módulos, apesar de estarem
disponíveis (e.g Pedidos), não sofrem mais alterações por parte de nossa equipe por que as alterações destinadas a estes
módulos são realizadas nas APIs especificas destes modulos.
</p>

## API de Pedidos
<b>DOCS:</b> <a href="https://rest.hub2b.com.br/orders-docs/index.html">https://rest.hub2b.com.br/orders-docs/index.html</a>
<p align="justify">
Essa API tem o intuito de realizar integrações com a Hub2b no que diz respeito a integração de pedidos. 
Ela foi desenvolvida com tecnologia capaz de realizar integração pedidos em tempos real, caso o ERP e o 
Marketplace em questão também dêem suporte à recepção de notificações através de URLs de Callback. 
</p>

## API de Preço e Estoque
<b>DOCS:</b> <a href="https://rest.hub2b.com.br/inventory-docs/index.html">https://rest.hub2b.com.br/inventory-docs/index.html</a>
<p align="justify">
Tem o intuito de realizar integração de preço e estoque. Foi desenvolvida com o mesmo intuito que a Orders API. 
</p>

Proximo Passo: [Autenticações](authentications.md)